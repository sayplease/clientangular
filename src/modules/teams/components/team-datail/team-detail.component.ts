import {Component, Input, OnInit} from '@angular/core';
import {Team} from "../../models/team";

@Component({
  selector: 'app-team-detail',
  templateUrl: './team-detail.component.html',
  styleUrls: ['./team-detail.component.scss']
})
export class TeamDetailComponent implements OnInit {

  // @ts-ignore
  @Input() team:Team;

  constructor() { }

  ngOnInit(): void {
  }

}
