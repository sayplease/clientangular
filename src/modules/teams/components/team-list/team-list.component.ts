import {Component, OnDestroy, OnInit} from '@angular/core';
import {TeamService} from "../../services/team.service";
import {Team} from "../../models/team";
import {Subject, takeUntil} from "rxjs";

@Component({
  selector: 'app-team-list',
  templateUrl: './team-list.component.html',
  styleUrls: ['./team-list.component.scss']
})
export class TeamListComponent implements OnInit, OnDestroy {

  public teams: Team[] = [];
  public cachedTeams: Team[] = [];
  public loadingTeams = false;
  public unsubscribe$ = new Subject<void>();

  constructor(
    private teamService: TeamService
  ) { }

  ngOnInit(): void {
    this.getTeams();
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  getTeams() {
    this.loadingTeams = true;
    this.teamService.getTeams()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
          // @ts-ignore
          this.teams = this.cachedTeams = resp.body
        },
        error => (this.loadingTeams = false));
  }



}
