export interface Team {
  id:number;
  name:string;
  createAt:Date;
}
