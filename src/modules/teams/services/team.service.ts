import { Injectable } from '@angular/core';
import {ClientApiService} from "../../helpers/services/client-api.service";
import {Team} from "../models/team";
import {NewTeam} from "../models/new-team";

@Injectable({
  providedIn: 'root'
})
export class TeamService {
  public routePrefix = '/api/teams';

  constructor(private httpService: ClientApiService) { }

  getTeams() {
    return this.httpService.getFullRequest<Team[]>(`${this.routePrefix}`);
  }

  createTeam(team:NewTeam) {
    return this.httpService.postFullRequest<Team>(`${this.routePrefix}`, team);
  }

  updateTeam(team:NewTeam) {
    return this.httpService.putFullRequest(`${this.routePrefix}`, team);
  }

  deleteTeam(id:number) {
    return this.httpService.deleteFullRequest(`${this.routePrefix}`, { id });
  }
}
