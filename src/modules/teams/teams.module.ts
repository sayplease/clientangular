import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TeamCreateComponent } from './components/team-create/team-create.component';
import { TeamEditComponent } from './components/team-edit/team-edit.component';
import { TeamListComponent } from './components/team-list/team-list.component';
import { TeamDetailComponent } from './components/team-datail/team-detail.component';
import {MaterialComponentsModule} from "../material-components/material-components.module";



@NgModule({
  declarations: [
    TeamCreateComponent,
    TeamEditComponent,
    TeamListComponent,
    TeamDetailComponent
  ],
  imports: [
    CommonModule,
    MaterialComponentsModule
  ]
})
export class TeamsModule { }
