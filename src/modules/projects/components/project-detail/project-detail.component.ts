import {Component, Input, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {Project} from "../../models/project";
import {MatDialog } from "@angular/material/dialog";





@Component({
  selector: 'app-project-detail',
  templateUrl: './project-detail.component.html',
  styleUrls: ['./project-detail.component.scss']
})
export class ProjectDetailComponent implements OnInit {

  // @ts-ignore
  @Input() public project: Project;

  constructor() { }

  ngOnInit(): void {
  }


 projectUpdate() {

 }

  projectDelete() {

  }
}
