import {Component, OnDestroy, OnInit} from '@angular/core';
import {ProjectService} from "../../services/project.service";
import {Project} from "../../models/project";
import {Subject, takeUntil} from "rxjs";
import {MatDialog, MatDialogConfig} from "@angular/material/dialog";
import {ProjectEditComponent} from "../project-edit/project-edit.component";

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.scss']
})
export class ProjectListComponent implements OnInit, OnDestroy {

  public projects: Project[] = [];
  public cachedProjects: Project[] = [];
  public loadingProjects = false;
  private unsubscribe$ = new Subject<void>();

  constructor(
    private projectService: ProjectService,
    private dialog: MatDialog
  ) { }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  ngOnInit(): void {
    this.getProjects();
  }

  public getProjects() {
    this.loadingProjects = true;
    this.projectService
        .getProjects()
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe(
          (resp) => {
            this.loadingProjects = false;
            // @ts-ignore
            this.projects = this.cachedProjects = resp.body;
            },
          error => (this.loadingProjects = false)
        );
  }

}
