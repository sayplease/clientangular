import {User} from "../../users/models/user";
import {Tasks} from "../../tasks/models/tasks";
import {Team} from "../../teams/models/team";

export interface Project {

  id:number;
  author:User;
  name:string;
  team:Team;
  description:string;
  deadline:Date;
  createAt:Date;
  tasks:Tasks[];

}
