import { Injectable } from '@angular/core';
import {ClientApiService} from "../../helpers/services/client-api.service";
import {Project} from "../models/project";
import {NewProject} from "../models/new-project";
import {EditProject} from "../models/edit-project";


@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  public routePrefix = '/api/projects';

  constructor(private httpService: ClientApiService) { }

  getProjects() {
      return this.httpService.getFullRequest<Project[]>(`${this.routePrefix}`);
  }

  createProject(project:NewProject) {
      return this.httpService.postFullRequest<Project>(`${this.routePrefix}`, project);
  }

  updateProject(project:EditProject) {
      return this.httpService.putFullRequest(`${this.routePrefix}`, project);
  }

  deleteProject(id:number) {
      return this.httpService.deleteFullRequest(`${this.routePrefix}`, { id });
  }


}
