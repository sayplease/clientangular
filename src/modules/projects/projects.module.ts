import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectCreateComponent } from './components/project-create/project-create.component';
import { ProjectEditComponent } from './components/project-edit/project-edit.component';
import { ProjectDetailComponent } from './components/project-detail/project-detail.component';
import { ProjectListComponent } from './components/project-list/project-list.component';
import {MaterialComponentsModule} from "../material-components/material-components.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatDialog, MatDialogModule} from "@angular/material/dialog";
import {OverlayModule} from "@angular/cdk/overlay";







@NgModule({
  declarations: [
    ProjectCreateComponent,
    ProjectEditComponent,
    ProjectDetailComponent,
    ProjectListComponent,



  ],
  exports: [
    ProjectListComponent,
    ProjectDetailComponent
  ],
  imports: [
    CommonModule,
    MaterialComponentsModule,
    FormsModule,
    ReactiveFormsModule,
    OverlayModule


  ]

})
export class ProjectsModule { }
