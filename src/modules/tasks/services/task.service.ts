import { Injectable } from '@angular/core';
import {ClientApiService} from "../../helpers/services/client-api.service";
import {NewTask} from "../models/new-task";
import {EditTask} from "../models/edit-task";
import {Tasks} from "../models/tasks";

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  public routePrefix = '/api/tasks';

  constructor(private httpService: ClientApiService) { }

  getTasks() {
    return this.httpService.getFullRequest<Tasks[]>(`${this.routePrefix}`);
  }

  createTask(task:NewTask) {
    return this.httpService.postFullRequest<Tasks>(`${this.routePrefix}`, task);
  }

  updateTask(task:EditTask) {
    return this.httpService.putFullRequest(`${this.routePrefix}`, task);
  }

  deleteTask(id:number) {
    return this.httpService.deleteFullRequest(`${this.routePrefix}`, { id });
  }
}
