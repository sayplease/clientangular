import {Project} from "../../projects/models/project";
import {User} from "../../users/models/user";
import {StateTask} from "./state-task";


export interface Tasks {

  project:Project;
  performer:User;
  name:string;
  description:string;
  state:StateTask;
  finishedAt?:Date;
  createAt:Date;
}
