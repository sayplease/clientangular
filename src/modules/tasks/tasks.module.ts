import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TaskCreateComponent } from './components/task-create/task-create.component';
import { TaskEditComponent } from './components/task-edit/task-edit.component';
import {MaterialComponentsModule} from "../material-components/material-components.module";
import { TaskListComponent } from './components/task-list/task-list.component';
import { TaskDetailComponent } from './components/task-detail/task-detail.component';




@NgModule({
  declarations: [
    TaskCreateComponent,
    TaskEditComponent,
    TaskListComponent,
    TaskDetailComponent
  ],
  imports: [
    CommonModule,
    MaterialComponentsModule,
  ]
})
export class TasksModule { }
