import {Component, Input, OnInit} from '@angular/core';
import {Tasks} from "../../models/tasks";

@Component({
  selector: 'app-task-detail',
  templateUrl: './task-detail.component.html',
  styleUrls: ['./task-detail.component.scss']
})
export class TaskDetailComponent implements OnInit {

  // @ts-ignore
  @Input() public task: Tasks;

  constructor() { }

  ngOnInit(): void {
  }

}
