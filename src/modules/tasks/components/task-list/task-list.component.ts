import { Component, OnInit } from '@angular/core';
import {Tasks} from "../../models/tasks";
import {Subject, takeUntil} from "rxjs";
import {TaskService} from "../../services/task.service";

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss']
})
export class TaskListComponent implements OnInit {

  public tasks: Tasks[] = [];
  public cachedTasks: Tasks[] = [];
  public loadingTasks = false;
  private unsubscribe$ = new Subject<void>();

  constructor(
    private taskService: TaskService
  ) { }

  ngOnInit(): void {
    this.getTasks();
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  getTasks() {
    this.loadingTasks = true;
    this.taskService.getTasks()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
          this.loadingTasks = false;
          // @ts-ignore
          this.tasks = this.cachedTasks = resp.body;
        },
        error => (this.loadingTasks = false));
  }
}
