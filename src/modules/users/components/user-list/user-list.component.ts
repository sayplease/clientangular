import {Component, OnDestroy, OnInit} from '@angular/core';
import {UserService} from "../../services/user.service";
import {User} from "../../models/user";
import {Subject, takeUntil} from "rxjs";

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit, OnDestroy {

  public users: User[] = [];
  public cachedUsers: User[] = [];
  public loadingUsers = false;
  private unsubscribe$ = new Subject<void>();

  constructor(
    private userService: UserService
  ) { }

  ngOnInit(): void {
    this.getUsers();
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  getUsers() {
    this.loadingUsers = true;
    this.userService.getUsers()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
          // @ts-ignore
          this.users = this.cachedUsers = resp.body;
        },
        error => (this.loadingUsers = false));
  }

}
