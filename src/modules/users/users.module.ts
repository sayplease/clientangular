import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserCreateComponent } from './components/user-create/user-create.component';
import { UserEditComponent } from './components/user-edit/user-edit.component';
import { UserDetailComponent } from './components/user-detail/user-detail.component';
import { UserListComponent } from './components/user-list/user-list.component';
import {MaterialComponentsModule} from "../material-components/material-components.module";



@NgModule({
  declarations: [
    UserCreateComponent,
    UserEditComponent,
    UserDetailComponent,
    UserListComponent
  ],
  imports: [
    CommonModule,
    MaterialComponentsModule
  ]
})
export class UsersModule { }
