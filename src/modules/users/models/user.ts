import {Team} from "../../teams/models/team";

export interface User {
  id:number;
  team:Team;
  firstName:string;
  lastName:string;
  email:string;
  birthDay:Date;
  createAt:Date;
}
