import { Injectable } from '@angular/core';
import {ClientApiService} from "../../helpers/services/client-api.service";
import {User} from "../models/user";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  public routePrefix = '/api/users';

  constructor(private httpService: ClientApiService) { }

  getUsers() {
    return this.httpService.getFullRequest<User[]>(`${this.routePrefix}`);
  }
}
