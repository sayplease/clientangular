import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {ProjectsModule} from "../modules/projects/projects.module";
import {HttpClientModule} from "@angular/common/http";
import { HomeComponent } from './home/home.component';
import {MaterialComponentsModule} from "../modules/material-components/material-components.module";
import {TasksModule} from "../modules/tasks/tasks.module";
import {TeamsModule} from "../modules/teams/teams.module";
import {UsersModule} from "../modules/users/users.module";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {ProjectEditComponent} from "../modules/projects/components/project-edit/project-edit.component";
import {MatDialogModule} from "@angular/material/dialog";






@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,


  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ProjectsModule,
    HttpClientModule,
    MaterialComponentsModule,
    TasksModule,
    TeamsModule,
    UsersModule,
    BrowserAnimationsModule,
    MatDialogModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [ProjectEditComponent]
})
export class AppModule { }
