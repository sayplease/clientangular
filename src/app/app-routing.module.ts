import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ProjectListComponent} from "../modules/projects/components/project-list/project-list.component";
import {TeamListComponent} from "../modules/teams/components/team-list/team-list.component";
import {UserListComponent} from "../modules/users/components/user-list/user-list.component";
import {TaskListComponent} from "../modules/tasks/components/task-list/task-list.component";

export const routes: Routes = [
 /* { path: '', component: HomeComponent, pathMatch: 'full'},*/
  { path: 'projects', component: ProjectListComponent},
  { path: 'project/tasks', component: TaskListComponent , pathMatch: 'full'},
  { path: 'teams', component: TeamListComponent, pathMatch: 'full'},
  { path: 'users', component: UserListComponent, pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
